﻿using MessageService.DbModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Dal.Mappings
{
    public class MessageMap : IEntityTypeConfiguration<MessageDb>
    {
        public void Configure(EntityTypeBuilder<MessageDb> builder)
        {
            builder = BaseMap.Build(builder);

            builder.Property(x => x.Additional).HasColumnName("additional");
            builder.Property(x => x.Theme).HasColumnName("theme");
            builder.Property(x => x.Text).HasColumnName("text");
            builder.Property(x => x.EventCategoryType).HasColumnName("event_category_type");
            builder.Property(x => x.Links).HasColumnName("links");
            builder.Property(x => x.Tags).HasColumnName("tags");
            builder.Property(x => x.EventDate).HasColumnName("event_date");

            builder.ToTable("messages");
        }
    }
}
