﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MessageService.DbModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Dal.Mappings
{
    public class ShippingInformationMap : IEntityTypeConfiguration<ShippingInformationDb>
    {
        public void Configure(EntityTypeBuilder<ShippingInformationDb> builder)
        {
            builder = BaseMap.Build(builder);

            builder.Property(x => x.Channel).HasColumnName("channel");
            builder.Property(x => x.EventCategory).HasColumnName("eventCategory");
            builder.Property(x => x.UserId).HasColumnName("userId");
            builder.Property(x => x.SendTo).HasColumnName("contactInfo");
            builder.Property(x => x.Status).HasColumnName("status");
            builder.Property(x => x.Error).HasColumnName("error");

            builder.HasOne(x => x.Message).WithMany(x => x.ShippingInformations);

            builder.ToTable("shipping_informations");
        }
    }
}
