﻿using MessageService.Dal.Mappings;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Dal
{
    public class Context : DbContext
    {
        private readonly string _connectionString;

        //public Context(string connectionString) : base()
        //{           
        //    _connectionString = connectionString;
        //}

        public Context() : base()
        {
            var connectionString = @"Data Source=Mega-db;Initial Catalog=PBook;persist security info=True;User id=sa; Password=04061608";
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MessageMap());
            modelBuilder.ApplyConfiguration(new ShippingInformationMap());
    

            //modelBuilder = RelationMapsConfigure(modelBuilder);
        }

        //public ModelBuilder RelationMapsConfigure(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.ApplyConfiguration(new UserEventCategoryMap());
        //    modelBuilder.ApplyConfiguration(new UserChannelMap());

        //    return modelBuilder;
        //}
    }  
}
