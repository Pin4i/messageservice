﻿using MessageService.DbModels;

namespace MessageService.Dal.Interfaces.Repositories
{
    public interface IMessageRepository : IBaseRepository<MessageDb>
    {
    }
}
