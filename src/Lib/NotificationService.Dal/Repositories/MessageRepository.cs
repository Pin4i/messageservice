﻿using MessageService.Dal.Interfaces.Repositories;
using MessageService.DbModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Dal.Repositories
{
    public class MessageRepository : BaseRepository<MessageDb>, IMessageRepository
    {
        public MessageRepository(Context context) : base(context)
        {
        }
    }
}
