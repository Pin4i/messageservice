﻿using MessageService.Be;
using MessageService.Be.Enums;
using MessageService.ApiModels.In.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessageService.Mappers
{
    public class MessageMapper
    {
        public static Message Map(MessageModel model)
        {
            return new Message()
            {
                Text = model.Text,
                Theme = model.Theme,
                EventCategory = model.EventCategory.ToString(),
                Additional = model.Additional,
                Links = model.Links,
                Tags = model.Tags,
                EventDate = model.EventDate,
                Channel = (ChannelEnum) model.Channel,
                SendTo = model.SendTo,
                UserId = model.UserId
            };
        }

        public static IEnumerable<Message> Map(IEnumerable<MessageModel> models)
        {
            return models.Select(Map).ToList();
        }
    }
}
