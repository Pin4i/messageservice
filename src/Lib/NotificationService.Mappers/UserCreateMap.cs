﻿using MessageService.ApiModels.In.User;
using MessageService.Be.Enums;
using MessageService.Be.Subscriptions;
using MessageService.Be.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessageService.Mappers
{
    public class UserCreateMap
    {
        public static UserCreate Map(UserCreateModel model)
        {
            return new UserCreate()
            {
                Login = model.Login,
                Password = model.Password,
                Contacts = model.Contacts.Select(x => new UserContactCreate()
                {
                    Type = (MessageService.Be.Enums.UserContactEnum)x.Type,
                    Value = x.Value,
                }).ToList(),
                Subscriptions = model.Subscriptions.Select(x => new SubscriptionCreate()
                {
                    ChannelType = (ChannelEnum)x.ChannelType,
                    EventCategoryType = (EventCategoryEnum)x.EventCategoryType
                }).ToList(),
            };
        }

        public static IEnumerable<UserCreate> Map(IEnumerable<UserCreateModel> models)
        {
            return models.Select(Map).ToList();
        }
    }
}
