﻿using MessageService.Be.Enums;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MessageService.Be
{
    public class Message
    {
        public DateTime EventDate { get; set; }
        public string Theme { get; set; }
        public string Text { get; set; }
        public string Additional { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public IDictionary<string, string> Links { get; set; }
        public string EventCategory { get; set; }
        public ChannelEnum Channel { get; set; }
        public Guid UserId { get; set; }
        public string SendTo { get; set; }
    }
}
