﻿using MessageService.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Be.Channels
{
    public class Channel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ChannelEnum Type { get; set; }
    }
}
