﻿using MessageService.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Be.Tapes
{
    public class EventCategory
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public EventCategoryEnum Type { get; set; }
    }
}
