﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Be.Enums
{
    public enum UserContactEnum
    {
        Email,
        Telegram,
        Phone
    }
}
