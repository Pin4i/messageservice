﻿using MessageService.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Be.Subscriptions
{
    public class Subscription
    {
        public ChannelEnum Channel { get; set; }
        public EventCategoryEnum EventCategory { get; set; }
    }
}
