﻿using MessageService.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Be.Subscriptions
{
    public class SubscriptionCreate
    {
       
            public ChannelEnum ChannelType { get; set; }
            public EventCategoryEnum  EventCategoryType { get; set; }
        
    }
}
