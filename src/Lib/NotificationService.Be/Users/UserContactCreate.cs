﻿using MessageService.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Be.Users
{
    public class UserContactCreate
    {
        public string Value { get; set; }
        public UserContactEnum Type { get; set; }
    }
}
