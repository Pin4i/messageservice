﻿using MessageService.Be.Channels;
using MessageService.Be.Subscriptions;
using MessageService.Be.Tapes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Be.Users
{
    public class UserCreate
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public IEnumerable<UserContactCreate> Contacts { get; set; }        
        public IEnumerable<SubscriptionCreate> Subscriptions { get; set; }




        //public IEnumerable<Channel> Channels { get; set; }
        //public IEnumerable<EventCategory> EventCategories { get; set; }


    }
}
