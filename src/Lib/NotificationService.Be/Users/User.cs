﻿using MessageService.Be.Enums;
using MessageService.Be.Subscriptions;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MessageService.Be.Users
{
    public class User
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public IEnumerable<UserContact> Contacts { get; set; }
        public IEnumerable<Subscription> Subscriptions { get; set; }
    }

    public class UserContact
    {
        public string Value { get; set; }
        public ChannelEnum Type { get; set; }
    }
}
