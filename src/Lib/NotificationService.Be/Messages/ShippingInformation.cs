﻿using MessageService.Be.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Be.Messages
{
    public class ShippingInformation
    {
        public ChannelEnum Channel { get; set; }
        public EventCategoryEnum EventCategory { get; set; }
        public ShippingStatusEnum Status { get; set; }
        public string Error { get; set; }
        public Guid UserId { get; set; }
        public string ContactInfo { get; set; }
        public DateTime? SendedDateTime { get; set; }
    }
}
