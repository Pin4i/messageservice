﻿using MessageService.ApiModels.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.ApiModels.In.User
{
    public class UserContactCreateModel
    {
        public ChannelEnumModel Type { get; set; }
        public string Value { get; set; }
    }
}
