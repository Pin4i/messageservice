﻿using MessageService.ApiModels.In.Subscription;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MessageService.ApiModels.In.User
{
    public class UserCreateModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        public IEnumerable<UserContactCreateModel> Contacts { get; set; }
        public IEnumerable<SubscriptionModel> Subscriptions { get; set; }

    }
}
