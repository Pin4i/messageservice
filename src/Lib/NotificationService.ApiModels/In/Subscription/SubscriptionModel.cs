﻿using MessageService.ApiModels.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.ApiModels.In.Subscription
{
    public class SubscriptionModel
    {
        public ChannelEnumModel ChannelType { get; set; }
        public EventCategoryEnumModel EventCategoryType { get; set; }
    }
}
