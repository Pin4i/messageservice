﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.ApiModels.In.Subscription
{
    public class SubscriptionCreateModel
    {
        public Guid UserId { get; set; }
        public SubscriptionModel Subscription { get; set; }

        //public IEnumerable<SubscriptionModel> Subscriptions { get; set; }
    }
}
