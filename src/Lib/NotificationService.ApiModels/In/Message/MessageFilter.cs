﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.ApiModels.In.Message
{
    public class MessageFilter
    {
        public DateTime BeginEventDate { get; set; }
        public DateTime EndEventDate { get; set; }
    }
}
