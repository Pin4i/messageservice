﻿using MessageService.ApiModels.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.ApiModels.In.Message
{
    public class MessageModel
    {
        public DateTime EventDate { get; set; }
        public ChannelEnumModel Channel { get; set; }
        public string Theme { get; set; }
        public string Text { get; set; }
        public string Additional { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public IDictionary<string, string> Links { get; set; }
        public EventCategoryEnumModel EventCategory { get; set; }
        public Guid UserId { get; set; }
        public string SendTo { get; set; }
    }
}
