﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.ApiModels.Enums
{
    public enum ChannelEnumModel
    {
        Email,
        Telegram,
    }
}
