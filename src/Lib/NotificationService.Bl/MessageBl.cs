﻿using MessageService.Be;
using MessageService.Be.Messages;
using MessageService.Be.Subscriptions;
using MessageService.Be.Users;
using MessageService.Bl.Interfaces;
using MessageService.Dal.Interfaces.Repositories;
using MessageService.DbModels;
using MessageService.Telegram.Models;
using MessageService.Telegram.Sender.Interfaces;
using Newtonsoft.Json;
using Services.Common.Bl.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelegramBot.Bl.Interfaces;

namespace MessageService.Bl
{
    public class MessageBl : IMessageBl
    {
        private readonly IServiceMessage _serviceSender;
        private readonly IMessageRepository _messageRepository;

        public MessageBl(IServiceMessage serviceSender, IMessageRepository messageRepository)
        {
            if (serviceSender == null) throw new ArgumentNullException("serviceSender");
            if (messageRepository == null) throw new ArgumentNullException("messageRepository");

            _serviceSender = serviceSender;
            _messageRepository = messageRepository;
        }

        public async Task SendMessage(IEnumerable<Message> messages)
        {
            foreach (var msg in messages)
            {
                await SendMessage(msg);
            }
        }

        public async Task SendMessage(Message message)
        {
            var messageDb = new MessageDb()
            {
                Additional = message.Additional,
                CreateDate = DateTime.Now,
                EventCategoryType = message.EventCategory,
                EventDate = message.EventDate,
                Id = Guid.NewGuid(),
                Theme = message.Theme,
                Text = message.Text,
                Tags = message.Tags.ToString(),
                Links = message.Links.ToString(),
                ShippingInformations = new List<ShippingInformationDb>()
                {
                    new ShippingInformationDb()
                    {
                        Id = Guid.NewGuid(),
                        CreateDate = DateTime.Now,
                        Channel = message.Channel.ToString(),
                        UserId = message.UserId,
                        EventCategory = message.EventCategory,
                        Status = DbModels.Enums.ShippingStatus.InProcessing,
                        SendTo = message.SendTo,
                    }
                }
            };

            var createdMessage = await _messageRepository.CreateAsync(messageDb);

            var result = await _serviceSender.SendText(new Services.Common.Models.CommonMessage());

            var newShippingInfo = new ShippingInformationDb()
            {
                Id = Guid.NewGuid(),
                CreateDate = DateTime.Now,
                Channel = message.Channel.ToString(),
                UserId = message.UserId,
                EventCategory = message.EventCategory,
                Status = result.Error ? DbModels.Enums.ShippingStatus.Error : DbModels.Enums.ShippingStatus.Sended,
                SendTo = message.SendTo,
                Error = result.Error ? result.ErrorText : null
            };

            messageDb.ShippingInformations.ToList().Add(newShippingInfo);
            await _messageRepository.UpdateAsync(messageDb);
        }
    }
}
