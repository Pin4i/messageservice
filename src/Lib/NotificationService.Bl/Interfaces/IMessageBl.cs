﻿using MessageService.Be;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MessageService.Bl.Interfaces
{
    public interface IMessageBl
    {
        Task SendMessage(Message message);
        Task SendMessage(IEnumerable<Message> messages);
    }
}
