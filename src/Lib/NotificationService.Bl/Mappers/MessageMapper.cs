﻿using MessageService.Be;
using MessageService.Be.Enums;
using MessageService.DbModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MessageService.Bl.Mappers
{
    public class MessageMapper
    {
        public static Message Map(MessageDb model)
        {
            return new Message()
            {
                Additional = model.Additional,
                EventCategory = model.EventCategoryType,
                Text = model.Text,
                Theme = model.Theme,
                Links = LinksConvert(model.Links),
                Tags = TagsConvert(model.Tags),
                EventDate = model.EventDate,
            };
        }

        public static IEnumerable<Message> Map(IEnumerable<MessageDb> models)
        {
            return models.Select(Map).ToList();
        }

        private static IEnumerable<string> TagsConvert(string jsonTags)
        {
            var tags = JsonConvert.DeserializeObject<IEnumerable<string>>(jsonTags);
            return tags;
        }
        private static IDictionary<string, string> LinksConvert(string jsonLinks)
        {
            var links = JsonConvert.DeserializeObject<IDictionary<string, string>> (jsonLinks);
            return links;
        }
    }
}
