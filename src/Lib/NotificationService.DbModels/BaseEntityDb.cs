﻿using System;

namespace MessageService.DbModels
{
    public class BaseEntityDb
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
