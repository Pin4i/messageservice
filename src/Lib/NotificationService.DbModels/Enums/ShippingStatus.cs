﻿namespace MessageService.DbModels.Enums
{
    public enum ShippingStatus
    {
        Created,
        InProcessing,
        Sended,
        Error,
    }
}
