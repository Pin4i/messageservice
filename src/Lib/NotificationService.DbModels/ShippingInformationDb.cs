﻿using MessageService.DbModels.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.DbModels
{
    public class ShippingInformationDb : BaseEntityDb
    {
        public string Channel { get; set; }
        public string EventCategory { get; set; }
        public Guid UserId { get; set; }
        public string SendTo { get; set; }
        public ShippingStatus Status { get; set; }
        public string Error { get; set; }

        public MessageDb Message { get; set; }
    }
}
