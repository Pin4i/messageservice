﻿using Microsoft.Extensions.DependencyInjection;
using MessageService.Bl;
using MessageService.Bl.Interfaces;
using MessageService.Dal;
using MessageService.Dal.Interfaces.Repositories;
using MessageService.Dal.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Configuration
{
    public class MessageServiceResolver
    {
        public static IServiceCollection Services;

        public static void Configure(IServiceCollection services)
        {
            PrepareBl(services);

            PrepareServices(services);
            PrepareDal(services);

            //services.AddTransient<ISettingsProvider, SettingsProvider>();

            Services = services;
        }

        private static void PrepareBl(IServiceCollection services)
        {
            services.AddTransient<IMessageBl, MessageBl>();

        }

        private static void PrepareDal(IServiceCollection services)
        {
            services.AddTransient<IMessageRepository, MessageRepository>();
            
            services.AddTransient<Context>();


        }

        private static void PrepareServices(IServiceCollection services)
        {
            //services.AddTransient<ITelegramSenderService, TelegramSenderService>();

        }
    }
}
