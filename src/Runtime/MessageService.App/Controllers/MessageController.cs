﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MessageService.ApiModels.In.Message;
using MessageService.Bl.Interfaces;
using MessageService.Mappers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MessageService.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageBl _messageBl;
        public MessageController(IMessageBl messageBl)
        {
            _messageBl = messageBl;
        }   

        [HttpPost("send")]
        public async Task SendMessage(MessageModel messageModel)
        {
            var message = MessageMapper.Map(messageModel);
            await _messageBl.SendMessage(message);
        }

        [HttpPost("manysend")]
        public async Task SendMessage(IEnumerable<MessageModel> messageModels)
        {
            var messages = MessageMapper.Map(messageModels);
            await _messageBl.SendMessage(messages);
        }
    }
}