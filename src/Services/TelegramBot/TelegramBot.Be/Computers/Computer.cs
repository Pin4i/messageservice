﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelegramBot.Be.Computers
{
    public class Computer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
