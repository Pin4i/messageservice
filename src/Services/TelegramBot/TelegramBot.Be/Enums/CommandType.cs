﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelegramBot.Be.Enums
{
    public enum CommandType
    {
        Start,
        Menu,
        CameraList,
        CameraId,
        WolList,
        PcId,
        RemindMe,
        None,
        Keyboard,
        Register
    }
}
