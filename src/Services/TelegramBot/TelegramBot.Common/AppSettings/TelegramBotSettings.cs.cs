﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace TelegramBot.Common.AppSettings
{
    public class TelegramBotSettings : ITelegramBotSettings
    {
        IConfiguration _configuration;

        public TelegramBotSettings(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public bool UseProxy => bool.Parse(_configuration.GetSection("UseProxy").Value);

        public bool ProxyAutoSettings => bool.Parse(_configuration.GetSection("ProxyAutoSettings").Value);

        public bool ProxyManualSettings => bool.Parse(_configuration.GetSection("ProxyManualSettings").Value);

        public string Token => _configuration.GetSection("Token").Value;

        public string ManualAddress => _configuration.GetSection("ManualProxyAddress").Value;

        public int ManualPort => int.Parse(_configuration.GetSection("ManualProxyPort").Value);

        public string ManualProxyUser => _configuration.GetSection("ManualProxyUser").Value;

        public string ManualProxyPassword => _configuration.GetSection("ManualProxyPassword").Value;

        public string ManualProxyType => _configuration.GetSection("ManualProxyType").Value;

        public string BalkonAddress => _configuration.GetSection("BalkonAddress").Value;
        public string BalkonUser => _configuration.GetSection("BalkonUser").Value;
        public string BalkonPassword => _configuration.GetSection("BalkonPassword").Value;

        public string UserInfoServiceUrl => _configuration.GetSection("UserInfoServiceUrl").Value;
    }
}
