﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelegramBot.Common.AppSettings
{
    public interface ITelegramBotSettings
    {
        string Token { get; }

        // PROXY
        bool UseProxy { get; }
        bool ProxyAutoSettings { get; }
        bool ProxyManualSettings { get; }


        // PROXY MANUAL SETTINGS
        string ManualProxyUser { get; }
        string ManualProxyPassword { get; }
        string ManualProxyType { get; }
        string ManualAddress { get; }
        int ManualPort { get; }

        string UserInfoServiceUrl { get; }
    }
}
