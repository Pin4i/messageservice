﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TelegramBot.Be.Cameras;

namespace TelegramBot.Operations.Interfaces
{
    public interface ICameraOperations
    {
        Task<IEnumerable<Camera>> GetCameras();

        Task<MemoryStream> GetPhoto(Guid cameraId);
    }
}
