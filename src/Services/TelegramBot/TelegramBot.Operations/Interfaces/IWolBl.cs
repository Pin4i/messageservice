﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TelegramBot.Be.Computers;

namespace TelegramBot.Operations.Interfaces
{
    public interface IWolOperations
    {
        Task<IEnumerable<Computer>> GetComputers();

        Task<Computer> GetComputer(Guid id);

        Task PowerOnComputer(Guid computerId);
    }
}
