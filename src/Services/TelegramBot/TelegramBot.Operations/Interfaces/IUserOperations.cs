﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot.Operations.Interfaces
{
    public interface IUserOperations
    {
        Task Register(long chatId, string login, string password);
    }
}
