﻿using Services.Common.Bl.Interfaces;
using Services.Common.Models;
using System;
using System.Threading.Tasks;
using TelegramBot.Be.Message;
using TelegramBot.Bl.Interfaces;
using TelegramBot.Service.Interfaces;

namespace TelegramBot.Bl
{
    public class TelegramMessageBl : ITelegramMessageBl, IServiceMessage
    {
        private readonly IBotService _botService;
        public TelegramMessageBl(IBotService botService)
        {
            _botService = botService;
        }

       

        public async Task<SendResult> SendText(CommonMessage message)
        {
            try
            {
                await _botService.SendMessage(int.Parse(message.To), message.Text);
                return new SendResult()
                {
                    Error = false,
                };
            }
            catch (Exception ex)
            {
                return new SendResult()
                {
                    Error = true,
                    ErrorText = ex.Message,
                };
            }
        }

        private string BuildMessage(TelegramMessage message)
        {
            //string htmlMsg = "<h3> Hello! </h3> <br><br> This is test message with <b>HTML</b> code #test #tags #formattingMessage";
            //string msg2 = "Hello, this is test message with maza faka tags #test #tags #mazaFaka";
            //string msg3 = "*Форматирование сообщения* Проверка форматирования сообщения. #test #tags #formattingMessage";
            //string msg = "*Форматирование сообщения*" + Environment.NewLine + Environment.NewLine +
            //    " Проверка форматирования сообщения. Можно написать какой нибудь текст, а можно не писать, по этому напишу кусок кода var users = GetUsers(); if (users.Any(x => x.Id == messageEventArgs.Message.From.Id)) { var message = messageEventArgs.Message;" + Environment.NewLine +
            //    "_примечания какие либо о чем либо все равно просто тест форматирования текст в сообщения в маркдауне все_" + Environment.NewLine +
            //    "[Сылка](yandex.ru)" + Environment.NewLine +
            //    "[Сылка](yandex.ru)" + Environment.NewLine +
            //    "[Сылка](yandex.ru)" + Environment.NewLine +
            //    "#tag1 #tag2 #tag3";

            var str = "*" + message.Theme + "*" + Environment.NewLine + Environment.NewLine +
                message.Text + Environment.NewLine +
                "_" + message.Additional + "_" + Environment.NewLine;

            if (message.Links != null)
            {
                foreach (var link in message.Links)
                {
                    str += link.Key + "(" + link.Value + ")" + Environment.NewLine;
                }
            }
            if (message.Tags != null)
            {
                foreach (var tag in message.Tags)
                {
                    str += tag + " ";
                }
            }
            return str;
        }



        //private Message PrepareMessage(Message message)
        //{
        //    message = PrepareTags(message);

        //    return message;
        //}

        //private Message PrepareTags(Message message)
        //{
        //    if (message.Tags == null || message.Tags.Count() == 0)
        //        return message;

        //    var tags = new List<string>();

        //    foreach (var tag in message.Tags)
        //    {
        //        if (tag[0] != '#')
        //        {
        //            tags.Add("#" + tag.ToLower());
        //        }
        //        else
        //        {
        //            tags.Add(tag.ToLower());
        //        }
        //    }

        //    message.Tags = tags;

        //    return message;
        //}




        //public async Task SendText(TelegramMessage message)
        //{
        //    bool sended = true;
        //    string error = "";
        //    var client = _botService.GetClient();
        //    try
        //    {
        //        await client.SendTextMessageAsync(message.To, BuildMessage(message), Telegram.Bot.Types.Enums.ParseMode.Markdown);
        //    }
        //    catch (System.Net.Sockets.SocketException ex)
        //    {
        //    }
        //    catch (System.Net.Http.HttpRequestException ex)
        //    {
        //        if (ex.InnerException != null)
        //        {
        //            if (ex.InnerException.InnerException != null)
        //            {
        //                if (ex.InnerException.InnerException is System.Net.Sockets.SocketException)
        //                {
        //                    _botService.Start();
        //                    client = _botService.GetClient();
        //                    await client.SendTextMessageAsync(message.To, BuildMessage(message), Telegram.Bot.Types.Enums.ParseMode.Markdown);
        //                }
        //            }
        //            if (ex.InnerException is System.Net.Sockets.SocketException)
        //            {
        //                _botService.Start();
        //                client = _botService.GetClient();
        //                await client.SendTextMessageAsync(message.To, BuildMessage(message), Telegram.Bot.Types.Enums.ParseMode.Markdown);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        sended = false;
        //        error = ex.Message + Environment.NewLine + ex.ToString() + Environment.NewLine + ex.StackTrace;
        //        //throw new Exception(ex.ToString());
        //    }
        //    return new SendResult()
        //    {
        //        Sended = sended,
        //        SendedDateTime = sended ? (DateTime?)DateTime.Now : null,
        //        Error = sended ? null : error,
        //    };
        //}
    }
}
