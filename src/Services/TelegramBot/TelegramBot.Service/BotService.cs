﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot.Be.Enums;
using TelegramBot.Common.AppSettings;
using TelegramBot.Operations.Interfaces;
using TelegramBot.Service.Helpers;
using TelegramBot.Service.Interfaces;

namespace TelegramBot.Service
{
    public class BotService : IBotService
    {   
        private static TelegramBotClient client;

        private readonly IProxyConfiguration _proxyConfiguration;
        private readonly ITelegramBotSettings _telegramSettings;
        private readonly ICameraOperations _cameraOperations;
        private readonly IWolOperations _wolOperations;
        private readonly IUserOperations _userOperations;
        private readonly ISenderHelper _senderHelper;

        public BotService(IProxyConfiguration proxyConfiguration,
            ITelegramBotSettings telegramSettings, ICameraOperations cameraOperations,
            IWolOperations wolOperations, IUserOperations userOperations,
            ISenderHelper senderHelper)
        {
            if (proxyConfiguration == null) throw new ArgumentNullException("proxyConfiguration");
            if (telegramSettings == null) throw new ArgumentNullException("telegramSettings");
            if (cameraOperations == null) throw new ArgumentNullException("cameraOperations");
            if (wolOperations == null) throw new ArgumentNullException("wolOperations");
            if (userOperations == null) throw new ArgumentNullException("userOperations");
            if (senderHelper == null) throw new ArgumentNullException("senderHelper");
            
            _proxyConfiguration = proxyConfiguration;
            _telegramSettings = telegramSettings;
            _cameraOperations = cameraOperations;
            _wolOperations = wolOperations;
            _userOperations = userOperations;
            _senderHelper = senderHelper;
        }


        #region CONTROLL LOGIC
        public void Start()
        {
            if (client != null)
            {
                client.StopReceiving();
                client = null;
            }

            client = CreateClient(_telegramSettings.Token, _telegramSettings.UseProxy, _telegramSettings.ProxyAutoSettings);
            var me = client.GetMeAsync().Result;         

            client.OnMessage += BotOnMessageReceived;
            client.OnMessageEdited += BotOnMessageReceived;
            client.OnCallbackQuery += BotOnCallbackQueryReceived;
            client.OnInlineQuery += BotOnInlineQueryReceived;
            client.OnInlineResultChosen += BotOnChosenInlineResultReceived;
            client.OnReceiveError += BotOnReceiveError;

            client.StartReceiving(Array.Empty<UpdateType>());
        }

        public void Stop()
        {
            client.StopReceiving();
        }

        public TelegramBotClient GetClient()
        {
            return client;
        }

        private TelegramBotClient CreateClient(string token, bool useProxy, bool useAutomateProxy)
        {
            TelegramBotClient telegramClient = null;

            if (useProxy)
            {
                if (useAutomateProxy)
                {
                    throw new NotImplementedException();
                }
                else
                {
                    var proxy = CreateSocks();
                    telegramClient = new TelegramBotClient(token, proxy);
                }

            }
            else
            {
                telegramClient = new TelegramBotClient(token);
            }

            client = telegramClient;

            return client;
        }
        #endregion
        

        #region CLIENT EVENTS

        private async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            if (message == null || message.Type != MessageType.Text) return;

            var type = GetType(message.Text);

            switch (type)
            {
                case CommandType.Start:
                    StartResponce(message.Chat.Id);
                    break;

                case CommandType.Register:
                    await RegisterResponce(message);
                    break;

                case CommandType.Menu:
                    MenuResponce(message);
                    break;
                                       
                default:
                    await _senderHelper.SendTextAsync(client, message.Chat.Id, ConstMessages.MenuChoses);
                    break;
            }
        }

        private async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;
            var type = GetType(callbackQuery.Data);

            if (type == CommandType.CameraList)
            {
                CameraListResponce(callbackQuery.From.Id);
            }
            if (type == CommandType.CameraId)
            {
                CameraIdResponce(callbackQueryEventArgs);
            }

            if (type == CommandType.WolList)
            {
                WolListResponce(callbackQuery.From.Id);
            }
            if (type == CommandType.PcId)
            {
                ComputerIdResponce(callbackQueryEventArgs);
            }
        }
        
        #region NOT USED

        private async void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs inlineQueryEventArgs)
        {
            Console.WriteLine($"Received inline query from: {inlineQueryEventArgs.InlineQuery.From.Id}");

            InlineQueryResultBase[] results = {
                new InlineQueryResultLocation(
                    id: "1",
                    latitude: 40.7058316f,
                    longitude: -74.2581888f,
                    title: "New York")   // displayed result
                    {
                        InputMessageContent = new InputLocationMessageContent(
                            latitude: 40.7058316f,
                            longitude: -74.2581888f)    // message if result is selected
                    },

                new InlineQueryResultLocation(
                    id: "2",
                    latitude: 13.1449577f,
                    longitude: 52.507629f,
                    title: "Berlin") // displayed result
                    {
                        InputMessageContent = new InputLocationMessageContent(
                            latitude: 13.1449577f,
                            longitude: 52.507629f)   // message if result is selected
                    }
            };

            await client.AnswerInlineQueryAsync(
                inlineQueryEventArgs.InlineQuery.Id,
                results,
                isPersonal: true,
                cacheTime: 0);
        }

        private void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            Console.WriteLine($"Received inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
        }
               
        private void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Console.WriteLine("Received error: {0} — {1}",
                receiveErrorEventArgs.ApiRequestException.ErrorCode,
                receiveErrorEventArgs.ApiRequestException.Message);
        }

        #endregion

        #endregion


        #region SEND LOGIC
        public async Task SendMessage(int to, string message, ParseMode parseMode = ParseMode.Markdown)
        {
            await _senderHelper.SendTextAsync(client, to, message, parseMode);
        }
        #endregion


        // ***************            *************** 
        // *************** RESPONCES  *************** 
        // ***************            *************** 

        #region Other responces

        private async void StartResponce(long chatId)
        {
            
            await client.SendTextMessageAsync(chatId, ConstMessages.StartResponce);
        }

        private async Task RegisterResponce(Message message)
        {
            var parameters = message.Text.Split(' ');
            if (parameters.Count() < 3)
            {
                await client.SendTextMessageAsync(message.From.Id, ConstMessages.RegisterResponce);

            }

            await client.SendTextMessageAsync(message.From.Id, "Регистрирую...");

            await _userOperations.Register(message.From.Id, parameters[1], parameters[2]);
        }

        private async void MenuResponce(Message message)
        {
            var parametrs = message.Text.Split(' ');
            if (parametrs.Count() == 1)
            {
                var menuItems = new InlineKeyboardMarkup(new[]
                                    {
                        new [] // first row
                        {
                            InlineKeyboardButton.WithCallbackData("Фото с камеры", "/" + CommandType.CameraList.ToString()),
                        },
                        new [] // second row
                        {
                            InlineKeyboardButton.WithCallbackData("Включить ПК", "/" + CommandType.WolList.ToString()),
                        }
                    });

                await client.SendTextMessageAsync(
                    message.Chat.Id,
                                        "Меню",
                    replyMarkup: menuItems);
            }
            if (parametrs.Count() == 2)
            {

            }
        }

        #endregion


        #region Camera responce

        private async void CameraListResponce(long chatId)
        {
            var cameras = await _cameraOperations.GetCameras();
            var root = new List<List<InlineKeyboardButton>>();

            foreach (var camera in cameras)
            {
                var items = new List<InlineKeyboardButton>();
                var item = InlineKeyboardButton.WithCallbackData(camera.Name, CommandType.CameraId.ToString() + ":" + camera.Id);
                items.Add(item);
                root.Add(items);
            }

            var menuItems = new InlineKeyboardMarkup(root);

            await client.SendTextMessageAsync(chatId, "*Выбор камеры*", replyMarkup: menuItems, parseMode: ParseMode.Markdown);
        }

        private async void CameraIdResponce(CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;
            var stringId = callbackQuery.Data.Split(":").Last();
            var id = Guid.Parse(stringId);

            var photo = await _cameraOperations.GetPhoto(id);

            await client.SendTextMessageAsync(callbackQuery.From.Id, "В разработке");
        }

        #endregion


        #region Wol responce

        private async void WolListResponce(long chatId)
        {
            var computers = await _wolOperations.GetComputers();
            var root = new List<List<InlineKeyboardButton>>();

            foreach (var computer in computers)
            {
                var items = new List<InlineKeyboardButton>();
                var item = InlineKeyboardButton.WithCallbackData(computer.Name, CommandType.PcId.ToString() + ":" + computer.Id);
                items.Add(item);
                root.Add(items);
            }

            var menuItems = new InlineKeyboardMarkup(root);

            await client.SendTextMessageAsync(chatId, "*Выбор ПК для включения*", replyMarkup: menuItems, parseMode: ParseMode.Markdown);
        }

        private async void ComputerIdResponce(CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;
            var stringId = callbackQuery.Data.Split(":").Last();
            var id = Guid.Parse(stringId);

            _wolOperations.PowerOnComputer(id);

            await client.SendTextMessageAsync(callbackQuery.From.Id, "Отправлен запрос на включение");
        }

        #endregion


        #region GET PROXY SETTINGS
        private dynamic CreateSocks()
        {
            return _proxyConfiguration.GetManualSettings(_telegramSettings.ManualAddress, _telegramSettings.ManualPort, _telegramSettings.ManualProxyUser, _telegramSettings.ManualProxyPassword);
        }

        #endregion




        /// 
        /// LOGIC
        /// 
        private CommandType GetType(string message)
        {
            var parametrs = message.Split(' ');

            switch (parametrs.First())
            {
                case "/start":
                    return CommandType.Start;

                case "/register":
                    return CommandType.Register;

                case "/menu":
                    return CommandType.Menu;

                case "/CameraList":
                    return CommandType.CameraList;

                case "/CameraId":
                    return CommandType.CameraId;

                case "/WolList":
                    return CommandType.WolList;

                case "/PcId":
                    return CommandType.PcId;

                case "/RemindMe":
                    return CommandType.RemindMe;



                case "/keyboard":
                    return CommandType.Keyboard;

                default:
                    return CommandType.None;
            }
        }




    }
}
