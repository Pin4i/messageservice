﻿using MihaZupan;
using System;
using System.Collections.Generic;
using System.Text;
using TelegramBot.Service.Interfaces;
using TelegramBot.Common.AppSettings;

namespace TelegramBot.Service
{
    public class ProxyConfiguration : IProxyConfiguration
    {
        private readonly ITelegramBotSettings _telegramSettings;
        public ProxyConfiguration(ITelegramBotSettings telegramSettings)
        {
            _telegramSettings = telegramSettings;
        }

        public HttpToSocks5Proxy GetManualSettings(string address, int port, string user, string password)
        {
            var Socks5ServerAddress = address;
            var Socks5ServerPort = port;
            var proxy = new HttpToSocks5Proxy(Socks5ServerAddress, Socks5ServerPort, user, password);

            // Allows you to use proxies that are only allowing connections to Telegram
            // Needed for some proxies
            //proxy.ResolveHostnamesLocally = true;

            return proxy;
        }
    }
}
