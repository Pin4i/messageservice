﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using TelegramBot.Service.Interfaces;

namespace TelegramBot.Service
{
    public class SenderHelper : ISenderHelper
    {
        public async Task SendTextAsync(TelegramBotClient client, long to, string message, ParseMode parseMode = ParseMode.Markdown)
        {            
            await client.SendTextMessageAsync(to, message, parseMode);
        }
    }
}
