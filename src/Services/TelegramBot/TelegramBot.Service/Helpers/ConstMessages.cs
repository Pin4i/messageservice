﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelegramBot.Service.Helpers
{
    public static class ConstMessages
    {

        public const string MenuChoses = @"/inline   - send inline keyboard
/keyboard - send custom keyboard
/photo    - send a photo
/request  - request location or contact";

        public const string StartResponce = "Введите логин и пароль в виде /register *login* *password* для регистрации";
        public const string RegisterResponce = "Не правильный формат сообщения. /register *login* *password* для регистрации";
        
    }
}
