﻿using MihaZupan;
using System;
using System.Collections.Generic;
using System.Text;

namespace TelegramBot.Service.Interfaces
{
    public interface IProxyConfiguration
    {
        HttpToSocks5Proxy GetManualSettings(string address, int port, string user, string password);
    }
}
