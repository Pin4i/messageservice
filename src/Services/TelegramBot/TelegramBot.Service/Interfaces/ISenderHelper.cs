﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace TelegramBot.Service.Interfaces
{
    public interface ISenderHelper
    {
        Task SendTextAsync(TelegramBotClient client, long to, string message, ParseMode parseMode = ParseMode.Markdown);
    }
}
