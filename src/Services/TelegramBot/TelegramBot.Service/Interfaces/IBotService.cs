﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.Enums;

namespace TelegramBot.Service.Interfaces
{
    public interface IBotService
    {
        Task SendMessage(int to, string message, ParseMode parseMode = ParseMode.Markdown);
    }
}
