﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Common.Models
{
    public class SendResult
    {
        public bool Error { get; set; }
        public string ErrorText { get; set; }
    }
}
