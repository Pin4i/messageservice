﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Common.Models
{
    public class CommonMessage
    {
        public string Theme { get; set; }
        public string Text { get; set; }
        public string Additional { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public IDictionary<string, string> Links { get; set; }
        public string To { get; set; }
    }
}
