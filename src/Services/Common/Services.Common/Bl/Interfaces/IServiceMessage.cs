﻿using Services.Common.Models;
using System.Threading.Tasks;

namespace Services.Common.Bl.Interfaces
{
    public interface IServiceMessage
    {
        Task<SendResult> SendText(CommonMessage message);
    }
}
