﻿using MessageService.Telegram.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MessageService.Telegram.Sender.Interfaces
{
    public interface ITelegramSenderService
    {
        Task<TelegramSendResult> SendText(TelegramMessage message);
    }
}
