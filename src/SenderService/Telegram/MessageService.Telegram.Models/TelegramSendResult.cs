﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Telegram.Models
{
    public class TelegramSendResult
    {
        public DateTime? SendedDateTime { get; set; }
        public bool Sended { get; set; }
        public string Error { get; set; }
        public Exception Ex { get; set; }
    }
}
