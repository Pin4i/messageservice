﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageService.Telegram.Models
{
    public class TelegramMessage
    {
        public string Theme { get; set; }
        public string Text { get; set; }
        public string Additional { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public IDictionary<string, string> Links { get; set; }
        public int MemberId { get; set; }
    }
}
